create table Customer_Info(
cust_id int not null primary key,
cust_name varchar(20),
phone_number int, 
email_id  varchar2(30)
);

create table Task(
task_id int not null primary key,
task_title varchar2(40),
task_date TIMESTAMP,
cust_fkid int references Customer_Info(cust_id) 
);

create table Task_List (
list_id int references Task(task_id),
task_list varchar2(100)
);